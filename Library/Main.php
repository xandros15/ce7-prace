<?php
/**
 * Description of Class
 *
 * @author ASUS
 */

namespace Library;

use Controller\PostController;
use Library\Login;
use Exception;
use R;

class Main
{
    /* login */

    protected $login = null;
    static public
        $charset = 'UTF-8',
        $language = 'pl-PL';

    public function __construct()
    {
        $this->setupGlobal();
        $this->setupRB();
        $this->setupLogin();
    }

    public function __destruct()
    {
        R::close();
    }

    public function run()
    {
        $postController = new PostController($this, $this->login);
        $postController->beforeRequest();
        $postController->haveRequest();
        $postController->afterRequest();
        echo $postController->loadIndex();
    }

    private function setupGlobal()
    {
        define('CONFIG_DIR', 'Config');
        define('VIEW_DIR', 'View');
        define('DEFAULT_EXTENTION', '.php');
        define('REDBEAN_MODEL_PREFIX', '\\Model\\');
        date_default_timezone_set('Europe/Warsaw');
    }

    private function setupRB()
    {
        $db = require_once implode(DIRECTORY_SEPARATOR, [ROOT_DIR, CONFIG_DIR, 'db' . DEFAULT_EXTENTION]);
        if (empty($db) || !is_array($db)) {
            throw new Exception('Error: filed to load db configuration');
        }
        require_once 'rb.php';
        R::setup("mysql:host={$db['host']};dbname={$db['name']}", $db['user'], $db['password']);
        R::debug(false);
        R::freeze(true);
    }

    private function setupLogin()
    {
        $param = require_once implode(DIRECTORY_SEPARATOR, [ROOT_DIR, CONFIG_DIR, 'login' . DEFAULT_EXTENTION]);
        session_start();
        $this->login = new Login();
        $this->login->setKey($param['key']);
        $this->login->setLogin($param['login']);
        $this->login->setPassword($param['password']);
    }
   
}
