<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Library;

class Controller
{

    protected static
        $main = null;
    protected
    /* @property $login \Library\Login */
        $scenario = 'default',
        $login = null;

    public function __construct($main, $login)
    {
        static::$main = $main;
        $this->login = $login;
    }

    protected function getScenario()
    {
        return $this->scenario;
    }
    
    protected function setScenario($scenario)
    {
        $this->scenario = $scenario;
    }

    protected function render($file, $vars = null)
    {
        if (is_array($vars) && !empty($vars)) {
            extract($vars);
        }
        ob_start();
        include implode(DIRECTORY_SEPARATOR, [ROOT_DIR, VIEW_DIR, $file . DEFAULT_EXTENTION]);
        return ob_get_clean();
    }
}
