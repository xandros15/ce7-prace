<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Library;

class Login
{

    private
        $login = '',
        $password = '',
        $key = '';

    /**
     * @var array Collection of error messages
     */
    public $errors = [];

    /**
     * @var array Collection of success / neutral messages
     */
    public $messages = [];

    public function goLogin($login, $haslo)
    {
        if (empty($login)) {
            $this->errors[] = "Username field was empty.";
        } elseif (empty($haslo)) {
            $this->errors[] = "Password field was empty.";
        } elseif ($login !== $this->login) {
            $this->errors[] = "This user does not exist.";
        } elseif ($haslo !== $this->getPassword()) {
            $this->errors[] = "Wrong password. Try again.";
        } else {
            $_SESSION['login'] = $login;
            $_SESSION['user_login_status'] = 1;
        }
    }

    /**
     * perform the logout
     */
    public function doLogout()
    {
        /* delete the session of the user */
        $_SESSION = [''];
        session_destroy();
        /*  return a little feeedback message */
        $this->errors[] = "You have been logged out.";
    }

    /**
     * simply return the current state of the user's login
     * @return boolean user's login status
     */
    public static function isUserLoggedIn()
    {
        if (isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] === 1) {
            return true;
        }
        /* default return */
        return;
    }

    public function setKey($key)
    {
        $this->key = (string) $key;
    }

    public function setLogin($login)
    {
        $this->login = (string) $login;
    }

    public function setPassword($password)
    {
        $iv = mcrypt_create_iv(
            mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM
        );

        $this->password = base64_encode(
            $iv .
            mcrypt_encrypt(
                MCRYPT_RIJNDAEL_256,
                hash('sha256', $this->key, true), (string) $password, MCRYPT_MODE_CBC, $iv
            )
        );
    }

    private function getPassword()
    {
        $data = base64_decode($this->password);
        $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC));

        return rtrim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_256,
                hash('sha256', $this->key, true), substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)), MCRYPT_MODE_CBC, $iv
            ), "\0"
        );
    }
}
