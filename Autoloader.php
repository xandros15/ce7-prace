<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Library;
/**
 * Description of autoloader
 *
 * @author ASUS
 */
class Autoloader
{

    /**
     * Includes the PHP-File for the specified class.
     * @param string $class The classname.
     * @author Daniel Siepmann <coding.layne@me.com>
     */
    public static function load($class)
    {
        $filename = __DIR__ . '/' . str_replace('\\', '/', $class) . '.php';
        if (file_exists($filename)) {
            return require $filename;
        }
        return false;
    }
}
