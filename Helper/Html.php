<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Helper;

use Library\Main;

/**
 * Description of Html
 *
 * @author ASUS
 */
class Html
{

    public static $voidElements = [
        'area' => 1,
        'base' => 1,
        'br' => 1,
        'col' => 1,
        'command' => 1,
        'embed' => 1,
        'hr' => 1,
        'img' => 1,
        'input' => 1,
        'keygen' => 1,
        'link' => 1,
        'meta' => 1,
        'param' => 1,
        'source' => 1,
        'track' => 1,
        'wbr' => 1,
    ];
    public static $attributeOrder = [
        'type',
        'id',
        'class',
        'name',
        'value',
        'href',
        'src',
        'action',
        'method',
        'selected',
        'checked',
        'readonly',
        'disabled',
        'multiple',
        'size',
        'maxlength',
        'width',
        'height',
        'rows',
        'cols',
        'alt',
        'title',
        'rel',
        'media',
    ];
    public static $dataAttributes = ['data', 'data-ng', 'ng'];

    public static function isLink($text)
    { 
        $list = explode(' ', str_replace(PHP_EOL, ' ', $text));
        foreach ($list as $link) {
            if (strpos($link, '.') === false) {
                continue;
            }
            $link = (stripos($link, 'http') !== 0) ? 'http://' . $link : $link;
            $parseHost = parse_url($link)['host'];
            $dot = strlen($parseHost) - strrpos($parseHost, '.');
            if ($dot > 2 and $dot < 6) {
                $text = str_replace($link, '<a href="' . $link . '" target="_blank">' . $link . '</a>', $text);
            }
        }

        return $text;
    }

    public static function roma($number)
    {
        $roman = [1 => 'I', 'II', 'III', 'IV', 'V', 'VI'];
        return isset($roman[(int) $number]) ? $roman[(int) $number] : null;
    }

    public static function encode($content, $doubleEncode = true)
    {
        return htmlspecialchars($content, ENT_QUOTES | ENT_SUBSTITUTE, empty(Main::$charset) ? Main::$charset : 'UTF-8', $doubleEncode);
    }

    public static function decode($content)
    {
        return htmlspecialchars_decode($content, ENT_QUOTES);
    }

    public static function tag($name, $content = '', $options = [])
    {
        $html = "<$name" . static::renderTagAttributes($options) . '>';
        return isset(static::$voidElements[strtolower($name)]) ? $html : "$html$content</$name>";
    }

    public static function jsFile($url, $options = [])
    {
        $options['src'] = $url;
        if (isset($options['condition'])) {
            $condition = $options['condition'];
            unset($options['condition']);
            return "<!--[if $condition]>\n" . static::tag('script', '', $options) . "\n<![endif]-->";
        } else {
            return static::tag('script', '', $options);
        }
    }

    public static function cssFile($url, $options = [])
    {
        if (!isset($options['rel'])) {
            $options['rel'] = 'stylesheet';
        }
        $options['href'] = $url;

        if (isset($options['condition'])) {
            $condition = $options['condition'];
            unset($options['condition']);
            return "<!--[if $condition]>\n" . static::tag('link', '', $options) . "\n<![endif]-->";
        } elseif (isset($options['noscript']) && $options['noscript'] === true) {
            unset($options['noscript']);
            return "<noscript>" . static::tag('link', '', $options) . "</noscript>";
        } else {
            return static::tag('link', '', $options);
        }
    }

    public static function renderTagAttributes($attributes)
    {
        if (count($attributes) > 1) {
            $sorted = [];
            foreach (static::$attributeOrder as $name) {
                if (isset($attributes[$name])) {
                    $sorted[$name] = $attributes[$name];
                }
            }
            $attributes = array_merge($sorted, $attributes);
        }


        $html = '';
        foreach ($attributes as $name => $value) {
            if (is_bool($value)) {
                if ($value) {
                    $html .= " $name";
                }
            } elseif (is_array($value)) {
                if (in_array($name, static::$dataAttributes)) {
                    foreach ($value as $n => $v) {
                        if (is_array($v)) {
                            $html .= " $name-$n='" . static::encode($v) . "'";
                        } else {
                            $html .= " $name-$n=\"" . static::encode($v) . '"';
                        }
                    }
                } else {
                    $html .= " $name='" . static::encode($value) . "'";
                }
            } elseif ($value !== null) {
                $html .= " $name=\"" . static::encode($value) . '"';
            }
        }
        return $html;
    }
}
