<?php
/* @var $model */
/* @var $this \Controller\PostController */

use Helper\Html;
use Library\Main;

$this->title = 'Rozpiska prac LO CE7';

?>
<!DOCTYPE html>
<html lang="<?= Main::$language; ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset="<?= Main::$charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= Html::encode($this->title) ?></title>
        <?= $this->actionHead() ?>
    </head>
    <body>
        <header class="_960">
            <div id="login" > 
                <?= $this->actionLoginForm(); ?>
            </div>
            <nav id="sem">
                <?= $this->actionSemestrMenu() ?>
            </nav>
        </header>
        <div id="wrapper" class="_960">
            <?= $this->render('SpecialInfo'); ?>
            <?= $this->actionPostForm(); ?>
            <?= $this->actionGrid(); ?>
            <noscript> 
            <section class="fall-back">
                <?= $this->actionFallback(); ?>
            </section>
            </noscript>
        </div>
        <footer>
            Design by Xandros &copy;lo7
        </footer>
    </body>
</html>