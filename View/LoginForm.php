<?php

use Helper\Html;
use Library\Login;
?>
<form method="POST" action="<?= Html::encode($_SERVER['PHP_SELF']) ?>">
    <?php if (Login::isUserLoggedIn()): ?> 
        <input name="logout" type="submit"  value="wyloguj" />
    <?php else: ?> 
        <span><label>Login:</label>&nbsp;<input name="loginName" type="text" value="" size="20" aria-required='true' /></span>
        <span><label>Hasło:</label>&nbsp;<input  name="password" type="password" value="" size="20" aria-required='true' /></span>
        <input name="login" type="submit"  value="zaloguj" />
    <?php endif ?>  
    <?php if (!empty($this->login->errors)): ?>
        <span><?= $this->login->errors[0]; ?></span>
    <?php endif; ?>
</form>