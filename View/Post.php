<?php
/* @var $model \Controller\PostController */

use Helper\Html;

?>
<article id="post-<?= $model->id ?>" class="post">
    <h2><?= ucfirst($model->subject); ?></h2>
    <h3><?= $model->type; ?></h3>
    <div>
        <h2>Treść:</h2>
        <?= nl2br(Html::decode(Html::isLink($model->text))); ?>
    </div>

    <?php if (!empty($model->info)): ?>
        <div>
            <h2>Dodatkowe Informacje:</h2>
            <?= nl2br(Html::decode(Html::isLink($model->info))); ?>
        </div>
    <?php endif; ?>
</article>