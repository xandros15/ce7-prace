<?php

use Controller\PostController as Post;

/* @var $this \Controller\PostController */
/* @var $singlePost \Controller\PostController */

?>

<section id="main-post" class="content">
    <?php
    if (!empty($model)):
        $class = [
            Post::TYPE_CREDITWORK => 'grid-sem',
            Post::TYPE_ISSUE => 'grid-exam',
            Post::TYPE_HOMEWORK => 'grid-home',
        ];

        ?>
        <article id="post-0"  class="post">
            <h2>Wybierz przedmiot:</h2>
        </article>
        <nav id="grid"> 
            <?php foreach ($model as $singlePost): ?> 
                <span id="postJs-<?= $singlePost->id ?>" class="<?= $class[$singlePost->type] ?>">
                    <?php if ($singlePost->end): ?>
                        <span class="ended">Zakończony</span>
                    <?php endif; ?>
                    <?php if ($this->isDayDelay($singlePost->updated_at)): ?>
                        <span class="updated">Zaktualizowany</span>
                    <?php endif; ?>
                    <?php if ($this->isDayDelay($singlePost->created_at)): ?>
                        <span class="created">Utworzony</span>
                    <?php endif; ?>
                    <span class="title">
                        <img src="./img/<?= $class[$singlePost->type] ?>.png" alt="<?= $singlePost->type ?>">
                        <?= $singlePost->subject ?>
                    </span>
                </span>
            <?php endforeach; ?>
        </nav>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#grid > span').click(function () {
                    var id = parseInt(this.id.split('-')[1]);
                    var content = $("#main-post .post");
                    $('html, body').animate({scrollTop: $('#wrapper').position().top}, 'slow');
                    $.post("index.php", {"ajax": true, "postId": id})
                            .done(function (responde) {
                                content.replaceWith(responde);
                            })
                            .fail(function () {
                                content.html('<p>Przepraszamy, ale strona jest chwilowo niedostępna</p>');
                            });
                });
            });
        </script>
    <?php else: ?>
        <p>Nie ma dodanych zagadnień w tym semestrze</p>
    <?php endif; ?>
</section>