<?php

use Helper\Html;
use Library\Login;

?>
<?php if (!empty($content)): ?>
    <meta name="description" content="<?= $content ?>" />
<?php endif; ?>
<?php if (!empty($metahead) && is_array($metahead)): ?>
    <?php foreach ($metahead as $file => $tag) : ?>
        <?php if (($tag == 'css')): ?>
            <?= Html::cssFile($file, ['type' => 'text/css', 'media' => 'all']) ?>
        <?php elseif (($tag == 'js')): ?>
            <?= Html::jsFile($file, ['type' => 'text/javascript']); ?>
        <?php endif; ?>
        <?= PHP_EOL ?>
    <?php endforeach; ?>
<?php endif; ?>
<noscript>
<style type="text/css">
    #grid {display:none;}
    #main-post {display:none;}
</style>
</noscript>
<?php if (Login::isUserLoggedIn()): ?>
    <script type="text/javascript">
        /* <![CDATA[ */
        var ajaxOpt = {'adminLogged': true};
        /* ]]> */
    </script>
<?php endif; ?>
