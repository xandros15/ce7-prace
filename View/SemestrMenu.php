<?php

use Helper\Html;

?>
<?php if (!empty($uniqueSemestr)): ?>
    <div>
        <h2>Wybierz semestr:</h2>
        <ul>
            <?php foreach ($uniqueSemestr as $i): ?>
            <li><a href="?SMT=<?= $i ?>" ><?= Html::roma($i) ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <h1>Semestr <?= $semestr ?></h1>
<?php else: ?>
    <div><p>Nie ma jeszcze obowiązujących zagadnień.</p></div>
<?php endif; ?>