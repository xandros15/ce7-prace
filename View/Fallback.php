<?php
/* @var $this \Controller\PostController */
/* @var $model \Controller\PostController */

?>
<?php if (!empty($model)): ?>
    <?php foreach ($model as $singleModel): ?>
        <?=
        $this->render('Post', [
            'model' => $singleModel
        ]);

        ?>
    <?php endforeach; ?>
<?php else: ?>
    <article id="post-0" class="post">
        <h1>Nie ma jeszcze obowiązyujących zagadnień do tego semestru.</h1>
    </article>
<?php endif; ?>