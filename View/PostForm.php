<?php

use Controller\PostController as Post;

/* @var $model \Controller\PostController */
/* @var $this \Controller\PostController */

?>
<section class="input">
    <?php $this->getError() ?>
    <form method="POST">
        <input type="hidden" name="id" value="<?= (!empty($model->id)) ? $model->id : ''; ?>">
        <span class="no-textarea">
            <label>Przedmiot:</label>
            <select name="subject">
                <?php foreach ($subject as $single): ?> 
                    <option value="<?= $single ?>" <?= (!empty($model->subject) && $single === $model->subject) ? 'selected' : null; ?>>
                        <?= $single ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </span>
        <span class="no-textarea">
            <label>Własny:</label>
            <input name="new_subject" type="text" value="" size="20" aria-required='true' />
        </span>
        <span class="no-textarea">
            <label>Semestr:</label>
            <select name="semestr"> 
                <?php for ($semestr = Post::MAX_SEMESTR; $semestr >= 1; $semestr--): ?> 
                    <option value="<?= $semestr; ?>" <?= (!empty($model->semestr) && $semestr == $model->semestr) ? 'selected' : null ?>>
                        <?= $semestr; ?>
                    </option> 
                <?php endfor; ?> 
            </select>  
        </span>
        <span class="no-textarea">
            <label>Rodzaj:</label>
            <select name="type">
                <?php foreach (static::getType() as $label => $value): ?>
                    <option value="<?= $value ?>" <?= (!empty($model->type) && $value == $model->type) ? 'selected' : null ?>><?= $label ?></option>
                <?php endforeach; ?>
            </select>
        </span>
        <span class="no-textarea">
            <label>Zakończony(?):</label>
            <input type="checkbox" name="end" value="1" <?= (!empty($model->end) && $model->end == 1) ? 'checked' : '' ?>>
        </span>
        <span><br>Treść:<br><textarea name="text" aria-required="true"><?= (!empty($model->text)) ? $model->text : null ?></textarea></span>
        <span><br>Notka:<br><textarea name="info" aria-required="true"><?= (!empty($model->info)) ? $model->info : null; ?></textarea></span><br>
        <input name="<?= (!empty($model->id)) ? 'update' : 'create'; ?>" type="submit" value="<?= (!empty($model->id)) ? 'edytuj' : 'stwórz'; ?>"/>
        <input name="delete" type="<?= (!empty($model->id)) ? 'submit' : 'hidden' ?>" value="usuń"/>
        <input type="button" onclick="cleanForm()" value="Resetuj">
    </form>
    <p>Możesz użyć następujących tagów oraz atrybutów
        <abbr>HTML</abbr>-a:
        <code>
            &lt;a href=&quot;&quot; title=&quot;&quot;&gt; &lt;abbr title=&quot;&quot;&gt; &lt;
            acronym title=&quot;&quot;&gt; &lt;b&gt; <br> &lt;blockquote cite=&quot;&quot;&gt; &lt;cite&gt; &lt;code&gt; &lt;
            del datetime=&quot;&quot;&gt; &lt;em&gt; &lt;i&gt; &lt;q cite=&quot;&quot;&gt; &lt;strike&gt; &lt;strong&gt; 
        </code>
        <br> polecane strony do uploadu: <br>tekstu: <a href="http://pastebin.com/">http://pastebin.com/</a>
        <br>obrazów/galerii: <a href="http://imgur.com/">http://imgur.com/</a>
        <br>plików/paczek: <a href="http://www.zippyshare.com/">http://www.zippyshare.com/</a> 
        lub <a href="https://mega.co.nz/">https://mega.co.nz/</a>
    </p> 
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#grid > span').click(function () {
            var id = parseInt(this.id.split('-')[1]);
            $.post('index.php', {'ajaxJson': true, 'postId': id})
                    .done(function (responde) {
                        results = JSON.parse(responde);
                        $('.input input[name=id]').val(id);
                        if ($('.input input[name=create]').length) {
                            $('.input input[name=create]').val('edytuj').attr('name', 'update');
                        }
                        $('.input input[name=end]').prop('checked', results.end);
                        $('.input select[name=subject]').val(results.subject);
                        $('.input select[name=semestr]').val(results.semestr);
                        $('.input select[name=type]').val(results.type);
                        $('.input textarea[name=text]').val((results.text.length > 0) ? results.text : '');
                        $('.input textarea[name=info]').val((results.info.length > 0) ? results.info : '');
                        $('.input input[name=delete]').attr('type', 'submit');
                    });
        });
    });
    function cleanForm() {
        $('.input input[name=id]').val(null);
        if ($('.input input[name=update]').length) {
            $('.input input[name=update]').val('stwórz').attr('name', 'create');
        }
        $('.input input[name=end]').prop('checked', 0);
        $('.input select[name=subject] option:first-child').attr('selected', 'selected');
        $('.input select[name=semestr] option:first-child').attr('selected', 'selected');
        $('.input select[name=type] option:first-child').attr('selected', 'selected');
        $('.input textarea[name=text]').val(null);
        $('.input textarea[name=info]').val(null);
        $('.input input[name=delete]').attr('type', 'hidden');
    }
</script>