<?php
chdir(__DIR__);
define('ROOT_DIR', __DIR__);
define('DEBUG', true);
if (DEBUG) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

include_once 'Autoloader.php';
spl_autoload_register('Library\Autoloader::load');

(new Library\Main())->run();