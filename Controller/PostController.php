<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controller;

use DateTime;
use Exception;
use Helper\Html;
use Library\Login;
use R;
use stdClass;

/**
 * This is the model class for table "zagadnienia_lo7".
 * 
 * 
 * @property integer $id 
 * @property string $subject
 * @property integer $semestr
 * @property string $type
 * @property string $text
 * @property string $info
 * @property bolean $end
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property class $main \Library\Main
 */
class PostController extends \Library\Controller
{

    const
        DAY_DELAY = 5;
    const
        MAX_SEMESTR = 6;
    const
        TYPE_ISSUE = 'zagadnienia',
        TYPE_HOMEWORK = 'praca domowa',
        TYPE_CREDITWORK = 'praca kontrolna';
    const
        TABLE = 'zagadnienia';

    public
        $validError = [],
        $post = null;
    protected
        $uniqueSemestr = [],
        $avaiableScenario = [
            'login',
            'logout',
            'create',
            'update',
            'delete',
            'ajax',
            'ajaxJson'
            ],
        $currentSemestr = 1;

    public static function getType($type = null)
    {
        $types = [
            self::TYPE_ISSUE => self::TYPE_ISSUE,
            self::TYPE_HOMEWORK => self::TYPE_HOMEWORK,
            self::TYPE_CREDITWORK => self::TYPE_CREDITWORK,
        ];
        if ($type !== null) {
            return isset($types[$type]) ? $types[$type] : false;
        }
        return $types;
    }

    public function isDayDelay($date)
    {
        return (!empty($date) && (new DateTime($date))->diff((new DateTime()), true)->d <= self::DAY_DELAY) ? true : false;
    }

    public function haveRequest()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)) {
            $this->post = new stdClass();
            foreach ($_POST as $name => $value) {
                $this->validation($name, $value);
            }
            if (!empty($this->validError)) {
                return;
            }

            $action = 'action' . ucfirst($this->getScenario());
            if (in_array($this->getScenario(), $this->avaiableScenario) && method_exists($this, $action)) {
                $this->{$action}();
            }
        }
    }

    public function beforeRequest()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)) {
            foreach ($this->avaiableScenario as $scenario) {
                if (array_key_exists($scenario, $_POST)) {
                    $this->setScenario($scenario);
                    break;
                }
            }
        }
    }

    public function afterRequest()
    {
        $this->uniqueSemestr = R::getCol('SELECT DISTINCT ( semestr ) FROM  ' . self::TABLE . ' ORDER BY semestr DESC');
        $this->currentSemestr = max($this->uniqueSemestr);
        if ($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET)) {
            if (in_array($_GET['SMT'], $this->uniqueSemestr)) {
                $this->currentSemestr = (int) $_GET['SMT'];
            }
        }
    }

    public function loadIndex()
    {
        return $this->render('Main');
    }

    public function actionCreate()
    {
        if (!Login::isUserLoggedIn()) {
            return;
        }
        $model = R::dispense(self::TABLE);
        $this->post->created_at = date('Y-m-d H:i:s');
        $model->import((array) $this->post, 'subject,semestr,type,text,info,end,created_at');
        R::store($model);
    }

    public function actionUpdate()
    {
        if (!Login::isUserLoggedIn()) {
            return;
        }
        $model = R::dispense(self::TABLE);
        $this->post->updated_at = date('Y-m-d H:i:s');
        $model->import((array) $this->post, 'id,subject,semestr,type,text,info,end,updated_at');
        R::store($model);
    }

    public function actionDelete()
    {
        if (!Login::isUserLoggedIn()) {
            return;
        }
        $model = R::load(self::TABLE, $this->post->id);
        if (empty($model)) {
            $this->validError['delete'] = 'Can\'t delete post, maby wrong ID';
            return false;
//            throw new Exception('Can\'t delete post, maby wrong ID');
        }
        R::trash($model);
    }

    public function actionHead($content = '')
    {
        return $this->render('Head', [
                'metahead' => [
                    'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css' => 'css',
                    './style.css' => 'css',
                    './Library/jquery-2.1.3.min.js' => 'js',
                    './Library/js.js' => 'js'
                ],
                'content' => $content
        ]);
    }

    public function actionLogin()
    {
        $this->login->goLogin($this->post->loginName, $this->post->password);
    }

    public function actionLogout()
    {
        $this->login->doLogout();
    }

    public function actionLoginForm()
    {
        return $this->render('LoginForm');
    }

    public function actionSemestrMenu()
    {
        return $this->render('SemestrMenu', [
                'uniqueSemestr' => $this->uniqueSemestr,
                'semestr' => $this->currentSemestr
        ]);
    }

    public function actionPostForm($id = null)
    {
        if (!Login::isUserLoggedIn()) {
            return;
        }
        $subject = R::getCol('SELECT DISTINCT (subject) FROM  ' . self::TABLE . ' ORDER BY  subject DESC');
        $model = (!empty($id)) ? R::load(self::TABLE, $id) : null;
        return $this->render('PostForm', [
                'model' => $model,
                'subject' => $subject
        ]);
    }

    public function actionGrid()
    {
        $model = R::find(self::TABLE, 'semestr = ? ORDER BY end ASC, subject DESC, created_at , updated_at DESC, type DESC', [$this->currentSemestr]);
        return $this->render('Grid', [
                'model' => $model
        ]);
    }

    public function actionFallback()
    {
        $model = R::find(self::TABLE, 'semestr = ?', [$this->currentSemestr]);
        return $this->render('Fallback', [
                'model' => $model
        ]);
    }

    public function actionAjax()
    {
        $model = R::load(self::TABLE, (int) $this->post->postId);
        if (empty($model)) {
            die();
        }
        echo $this->render('Post', [
            'model' => $model]);
        die();
    }

    public function actionAjaxJson()
    {
        $model = R::load(self::TABLE, (int) $this->post->postId);
        if (empty($model) || !Login::isUserLoggedIn()) {
            die();
        }
        $model->text = Html::decode($model->text);
        $model->info = Html::decode($model->info);
        echo json_encode((array) $model->getProperties(), JSON_NUMERIC_CHECK);
        die();
    }

    public function getError()
    {
        if (!empty($this->validError)) {
            $error = 'wystąpił błąd: <ul>';
            foreach ($this->validError as $name => $value) {
                $error .= "<li>$name: $value</li>";
            }
            echo $error . '</ul>';
        }
    }

    private function rule()
    {
        return [
            [['login', 'password', 'new_subject', 'subject', 'text', 'info'], 'string'],
            [['login', 'password', 'new_subject', 'subject', 'text', 'info'], 'trim'],
            [['login', 'password', 'new_subject', 'subject', 'text', 'info'], 'stripslashes'],
            [['login', 'password'], 'htmlspecialchars'],
            ['semestr', 'integer', 'min' => 1, 'max' => 1],
            ['new_subject', 'match', 'pattern' => '/([a-zżźćńółęąś ]+|)/i'],
            ['subject', 'match', 'pattern' => '/([a-zżźćńółęąś ]+|)/i'],
            ['text', 'string', 'min' => 1],
            ['end', 'boolean'],
        ];
    }

    private function validation($name, $value)
    {
        $rules = $this->rule();
        foreach ($rules as $rule) {

            if (is_string($rule[0]) && !empty($rule[0])) {
                $ruleName = [$rule[0]];
            } elseif (is_array($rule[0]) && !empty($rule[0])) {
                $ruleName = $rule[0];
            } else {
                throw new Exception('Wrong rule name');
            }

            if (!in_array($name, $ruleName)) {
                continue;
            }

            $validator = $rule[1];
            $option = array_slice($rule, 2);

            $basicValidators = [
                'boolean' => FILTER_VALIDATE_BOOLEAN,
                'integer' => FILTER_VALIDATE_INT,
                'email' => FILTER_VALIDATE_EMAIL,
                'url' => FILTER_VALIDATE_URL
            ];
            if (is_callable($validator)) {
                $valueAfterValid = $validator($value);
            } elseif (array_key_exists($validator, $basicValidators)) {
                $valueAfterValid = filter_var($value, $basicValidators[$validator]);
            } elseif (function_exists($validator)) {
                $valueAfterValid = $validator($value);
            } elseif (($validator == 'match') && isset($option['pattern'])) {
                $valueAfterValid = (preg_match($option['pattern'], $value) === 1) ? $value : false;
            } elseif ($validator == 'string') {
                $valueAfterValid = (is_string($value)) ? $value : false;
            }
            if (isset($option['max'])) {
                $valueAfterValid = ($option['max'] <= strlen($value)) ? $valueAfterValid : false;
            }
            if (isset($option['min'])) {
                $valueAfterValid = ($option['min'] <= strlen($value)) ? $valueAfterValid : false;
            }


            if ($valueAfterValid === false) {
                $this->validError[$name] = 'Błąd walidacji';
            }
        }
        if (empty($this->validError[$name])) {
            $this->post->{$name} = $value;
        }
    }
}
