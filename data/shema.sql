-- phpMyAdmin SQL Dump
-- version 4.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 12 Cze 2015, 17:32
-- Wersja serwera: 5.5.41-0+wheezy1
-- Wersja PHP: 5.6.6-1~dotdeb.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Baza danych: `lo7`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zagadnienia`
--

CREATE TABLE IF NOT EXISTS `zagadnienia` (
  `id` int(3) NOT NULL,
  `subject` varchar(64) COLLATE utf8_polish_ci NOT NULL,
  `semestr` int(1) NOT NULL,
  `type` set('zagadnienia','praca domowa','praca kontrolna') COLLATE utf8_polish_ci NOT NULL,
  `text` text COLLATE utf8_polish_ci NOT NULL,
  `info` text COLLATE utf8_polish_ci,
  `end` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `zagadnienia`
--
ALTER TABLE `zagadnienia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `zagadnienia`
--
ALTER TABLE `zagadnienia`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT;COMMIT;
